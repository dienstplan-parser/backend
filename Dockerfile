FROM openjdk:slim-bullseye
ARG tag
COPY build/libs/dienstplan-parser-backend-$tag.jar /app.jar

VOLUME /temp_scan
VOLUME /error_logs

RUN apt-get update
RUN apt-get install -y tesseract-ocr
RUN apt-get install -y tesseract-ocr-deu
RUN apt-get install -y poppler-utils


#config.json must be mounted via volume
CMD ["java", "-jar", "app.jar", "config.json"]