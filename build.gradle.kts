plugins {
    kotlin("jvm") version "1.5.30"
    id("net.nemerosa.versioning").version("2.15.1")
    java
}

group = "fp.dienstplan"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.5.21")
    implementation("io.javalin:javalin:4.2.0")
    implementation("org.slf4j:slf4j-simple:2.0.0-alpha1")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.1")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.13.1")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
    implementation("com.google.code.gson:gson:2.8.9")

    implementation("io.insert-koin:koin-core:3.1.4")
    testImplementation("io.insert-koin:koin-test:3.1.4")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")
}

tasks.getByName<net.nemerosa.versioning.tasks.VersionFileTask>("versionFile") {
    this.file = File(buildDir, "resources/main/version.properties")
}

tasks.getByName("classes"){
    this.dependsOn.add(tasks.getByName("versionFile"))
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

tasks.getByName<Jar>("jar") {
    manifest {
        this.attributes["Main-Class"] = "fp.dienstplan.MainKt"
    }

    this.from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    this.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}