set -e
if [ -z "$1" ]
then
  echo "Please provide tag to build docker image for!"
  exit 1
fi

set_color (){
 tput setaf 2
 tput bold
}

reset_color (){
  tput sgr0
}

echo_colored (){
  set_color
  echo "$1"
  reset_color
}

echo_colored "Checking out tag"
git checkout tags/"$1"

echo_colored "Running gradle build"
./gradlew build

echo_colored "Building docker image"
docker build --build-arg tag="$1" -t dienstplan-parser-backend:"$1" .