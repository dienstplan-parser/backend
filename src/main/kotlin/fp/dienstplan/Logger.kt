package fp.dienstplan

import java.time.LocalDateTime

class Logger(private val prefix: String) {
    fun print(type: Type, format: String, vararg args: Any?) {
        println("[$type][$prefix @ ${LocalDateTime.now()}] ${format.format(*args)}")
    }

    enum class Type {
        Info, Error;
    }
}