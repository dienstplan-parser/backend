package fp.dienstplan

import fp.dienstplan.config.Config
import fp.dienstplan.queue.JobQueueHandler
import fp.dienstplan.quicklink.QuickLinkHandler
import fp.dienstplan.util.toInstance
import fp.dienstplan.util.versionProperties
import fp.dienstplan.web.javalinModule
import io.javalin.Javalin
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import org.koin.dsl.module
import java.io.File

fun main(args: Array<String>) {
    if (args.isEmpty()) error("Please provide path to config!")
    println("Launching version ${versionProperties?.get("VERSION_FULL")}..")

    startKoin {
        modules(
            module {
                single<Config> { File(args[0]).toInstance() }
            },
            module {
                single {
                    get<Config>().run {
                        JobQueueHandler(
                            this.concurrentJobs,
                            this.tesseractExecutable,
                            this.pdfToCairoExecutable,
                            this.errorLogDir
                        )
                    }
                }
            },
            module {
                single {
                    QuickLinkHandler(get<Config>().quickLinkExpireTimeMs)
                }
            },
            javalinModule
        )
    }

    object : KoinComponent {
        val config by inject<Config>()

        init {
            listOf(config.tesseractExecutable, config.pdfToCairoExecutable).forEach {
                if (!File(it).exists()) {
                    error("Executable \"$it\" does not exist!")
                }
            }
        }
    }

    object : KoinComponent {
        val javalin by inject<Javalin>()
        val config by inject<Config>()

        init {
            javalin.start(config.webPort)
        }
    }

    object : KoinComponent {
        val jobQueueHandler by inject<JobQueueHandler>()

        init {
            Thread(jobQueueHandler).start()
        }
    }
}
