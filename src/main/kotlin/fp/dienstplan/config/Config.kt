package fp.dienstplan.config

import com.fasterxml.jackson.annotation.JsonAutoDetect
import java.io.File

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class Config(
    val debug: Boolean = true,
    val concurrentJobs: Int = 2,
    private val tempScanDirPath: String = "temp_scan",
    private val errorLogDirPath: String = "error_logs",
    val webPort: Int = 5342,
    val tesseractExecutable: String = "/usr/bin/tesseract",
    val pdfToCairoExecutable: String = "/usr/bin/pdftocairo",
    val maxFileSizeBytes: Long = 1024 * 1024 * 1024 * 50L,
    val quickLinkExpireTimeMs: Long = 60 * 60 * 1000L
) {
    @Transient
    val tempScanDir = File(tempScanDirPath).apply { this.mkdirs() }

    @Transient
    val errorLogDir = File(errorLogDirPath).apply { this.mkdirs() }
}