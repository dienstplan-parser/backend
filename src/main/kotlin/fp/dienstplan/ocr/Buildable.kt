package fp.dienstplan.ocr

interface Buildable<T> {
    fun build(): T
}
