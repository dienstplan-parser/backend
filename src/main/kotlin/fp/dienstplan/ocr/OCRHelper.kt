package fp.dienstplan.ocr

import fp.dienstplan.util.launchProcess
import java.io.File

class OCRHelper<P, B : Buildable<P>>(
    private val baseExecutable: String = "/usr/bin/tesseract",
    private val dropBlank: Boolean = true,
    private val languagePack: String = "deu",
    private val addData: (B, String) -> Unit
) {
    class OCRException(val errorLog: List<String>) : Exception()

    @Throws(OCRException::class)
    fun run(file: File, builder: B): P {
        val errorLog = mutableListOf<String>()

        val exitCode = launchProcess(
            baseExecutable,
            file.absolutePath,
            "stdout",
            "-l", languagePack
        ) { line ->
            errorLog.add(line)

            if ((line.isNotBlank() || !dropBlank)) {
                addData(builder, line)
            }
        }

        if (exitCode != 0) {
            throw OCRException(errorLog)
        }

        return builder.build()
    }
}


fun String.containsAny(list: List<String>): Boolean {
    for (item in list) if (this.contains(item)) return true
    return false
}

fun main() {
    val weekDaysGerman = listOf("Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag")

    val (status, plan) = OCRHelper<Plan, Plan.Builder>(addData = { builder, line ->
        if (line.containsAny(weekDaysGerman)) {
            builder.schedule.add(line)
        } else if (line.contains("Periode")) {
            builder.setPeriod(line)
        }
    }).run(File("dienstplan_december_2021.pdf.png-1.png"), Plan.Builder())


//    ZiviParser.parse(plan.schedule, pla)
}

