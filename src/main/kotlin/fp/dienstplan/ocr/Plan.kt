package fp.dienstplan.ocr

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

data class Plan private constructor(val period: LocalDate, val schedule: List<String>) {

    class Builder(
        private var period: LocalDate = LocalDate.now(),
        val schedule: MutableList<String> = mutableListOf()
    ) : Buildable<Plan> {

        companion object {
            private val DTF = DateTimeFormatter.ofPattern("dd.MM.yyyy")
            private val PERIOD_REGEX = ".*?(\\d{1,2}\\.\\d{1,2}\\.\\d{4}).*".toRegex()
        }

        fun setPeriod(line: String) {
            PERIOD_REGEX.matchEntire(line)?.let { result ->
                val (_, dateStr) = result.groupValues

                this.period = try {
                    LocalDate.parse(dateStr, DTF)
                } catch (e: DateTimeParseException) {
                    LocalDate.now()
                }
            }
        }

        override fun build(): Plan {
            return Plan(period, schedule.toList())
        }
    }
}