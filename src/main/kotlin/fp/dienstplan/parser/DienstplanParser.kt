package fp.dienstplan.parser

import fp.dienstplan.parser.output.OutputBuilder
import fp.dienstplan.parser.output.ical.ICalBuilder
import fp.dienstplan.parser.output.json.JsonBuilder
import fp.dienstplan.parser.timezone.Timezone
import fp.dienstplan.parser.timezone.europe.TzVienna
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

object DienstplanParser {
    private const val SEPARATOR = " "
    private const val FREE_DAY = "frei"
    private const val HOLIDAYS = "Urlaub"

    private val TIME_DTF: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")

    private val DIENST_LINE_REGEX =
        "^(\\d{1,2})\\s{1}(?:(?:\\bMontag\\b)|(?:\\bDienstag\\b)|(?:\\bMittwoch\\b)|(?:\\bDonnerstag\\b)|(?:\\bFreitag\\b)|(?:\\bSamstag\\b)|(?:\\bSonntag\\b))\\s(.*)\\s{1}\\d{1}\\s{1}(\\d{2}:\\d{2})\\s?-\\s?(\\d{2}:\\d{2})\\s{1}\\d{1,2},\\d{2}$".toRegex()

    fun parse(
        lines: List<String>,
        month: Int,
        year: Int,
        includeFreeDays: Boolean = false,
        includeHolidays: Boolean = false,
        timezone: Timezone = TzVienna,
        builder: OutputBuilder = ICalBuilder()
    ): String {
        builder.timezone = timezone
        builder.addEvents(lines.map { it.sanitizeLine() }.mapNotNull { line ->
            val tokens = line.split(SEPARATOR).filter { it.isNotBlank() }
            if (tokens.size >= 4) {
                val date = LocalDate.of(year, month, tokens[0].toInt())

                if (tokens.size == 4 || line.contains(FREE_DAY)) {
                     if (includeFreeDays) Event.FreeDay(date) else null
                } else  if (tokens.size == 5 || line.contains(HOLIDAYS)) {
                    if(includeHolidays) Event.Holidays(date) else null
                } else {
                    val result = DIENST_LINE_REGEX.matchEntire(line)!!
                    val (_, _, type, start, end) = result.groupValues
                    Event.WorkDay(type, LocalTime.parse(start, TIME_DTF), LocalTime.parse(end, TIME_DTF), date)
                }
            } else null
        })

        return builder.build()
    }
}

fun main() {
    val text = DienstplanParser.parse(
        listOf(

        ), 10, 2021, includeFreeDays = true, includeHolidays = true, builder = JsonBuilder()
    )

    println(text)
}

fun StringBuilder.newLine(text: String?): StringBuilder = this.append(text).append("\n")

fun <T> List<T>.getReverse(i: Int): T {
    return this[this.size - 1 - i]
}

fun <T> List<T>.lastN(n: Int): List<T> {
    if (n >= this.size) throw NoSuchElementException("n must not be longer than list")

    return with(this.size) {
        subList(this - 1 - n, this)
    }
}

fun String.sanitizeLine(): String {
    return this
        .replace(".", "")
        .replace("|", "")
        .replace(" |", "")
        .replace("| ", "")
        .replace(" | ", "")
        .replace("©", "")
        .replace(" ©", "")
        .replace("_", "")
}