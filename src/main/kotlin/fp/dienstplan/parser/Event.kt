package fp.dienstplan.parser

import fp.dienstplan.parser.output.EventFormat
import java.time.LocalDate
import java.time.LocalTime

sealed class Event(val localDate: LocalDate) {

    abstract fun provideSummary(): String
    fun provideStart(eventFormat: EventFormat) = eventFormat.provideStart(this)
    fun provideEnd(eventFormat: EventFormat) = eventFormat.provideEnd(this)

    class WorkDay(
        val type: String,
        val startAt: LocalTime,
        val endAt: LocalTime,
        localDate: LocalDate
    ) : Event(localDate) {

        override fun provideSummary(): String {
            return type
        }

    }

    class FreeDay(localDate: LocalDate) : Event(localDate) {
        override fun provideSummary(): String {
            return "Freier Tag"
        }
    }

    class Holidays(localDate: LocalDate) : Event(localDate){
        override fun provideSummary(): String {
            return "Urlaub"
        }
    }
}