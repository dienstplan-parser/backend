package fp.dienstplan.parser.output

import fp.dienstplan.parser.Event

interface EventFormat {
    fun provideStart(event: Event): String?
    fun provideEnd(event: Event): String?
}