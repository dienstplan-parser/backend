package fp.dienstplan.parser.output

import fp.dienstplan.parser.Event
import fp.dienstplan.parser.timezone.Timezone

abstract class OutputBuilder {
    protected val events = mutableListOf<Event>()
    lateinit var timezone: Timezone

    fun addEvent(event: Event) {
        this.events.add(event)
    }

    fun addEvents(events: List<Event>) = this.events.addAll(events)

    abstract fun build(): String
}