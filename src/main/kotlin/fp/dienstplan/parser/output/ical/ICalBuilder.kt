package fp.dienstplan.parser.output.ical

import fp.dienstplan.parser.Event
import fp.dienstplan.parser.newLine
import fp.dienstplan.parser.output.EventFormat
import fp.dienstplan.parser.output.OutputBuilder
import java.time.format.DateTimeFormatter

class ICalBuilder : OutputBuilder() {
    override fun build(): String {
        return buildString {
            this.newLine("BEGIN:VCALENDAR")
            this.newLine("VERSION:2.0")
            this.newLine("PRODID:-//dienstplanparser")

            timezone.build(this)
            events.forEach { event ->
                this.newLine("BEGIN:VEVENT")
                this.append("DTSTART").newLine(event.provideStart(ICalEventFormat))
                this.append("DTEND").newLine(event.provideEnd(ICalEventFormat))
                this.append("SUMMARY:").newLine(event.provideSummary())
                this.newLine("END:VEVENT")
            }

            this.newLine("END:VCALENDAR")
        }
    }

    object ICalEventFormat : EventFormat {
        private val ICAL_DATETIME_FORMAT: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss")
        private val ICAL_DATE_FORMAT: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd")

        override fun provideStart(event: Event): String {
            return when (event) {
                is Event.WorkDay -> ":" + ICAL_DATETIME_FORMAT.format(event.localDate.atTime(event.startAt))
                else -> ";VALUE=DATE:" + ICAL_DATE_FORMAT.format(event.localDate)
            }
        }

        override fun provideEnd(event: Event): String {
            return when (event) {
                is Event.WorkDay -> ":" + ICAL_DATETIME_FORMAT.format(event.localDate.atTime(event.endAt))
                else -> ";VALUE=DATE:" + ICAL_DATE_FORMAT.format(event.localDate)
            }
        }
    }
}