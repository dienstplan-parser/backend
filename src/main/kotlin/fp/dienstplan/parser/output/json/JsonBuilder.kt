package fp.dienstplan.parser.output.json

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import fp.dienstplan.parser.Event
import fp.dienstplan.parser.output.EventFormat
import fp.dienstplan.parser.output.OutputBuilder

class JsonBuilder : OutputBuilder() {
    override fun build(): String {
        val array = JsonArray()
        events.forEach {
            array.add(JsonObject().apply {
                this.addProperty("type", it.provideSummary())
                this.addProperty("date", it.localDate.toString())
                this.addNonNullProperty("start", it.provideStart(JsonEventFormat))
                this.addNonNullProperty("end", it.provideEnd(JsonEventFormat))
            })
        }

        return array.toString()
    }

    private fun JsonObject.addNonNullProperty(key: String, value: String?) {
        if (value != null) this.addProperty(key, value)
    }

    object JsonEventFormat : EventFormat {
        override fun provideStart(event: Event) = when (event) {
            is Event.WorkDay -> event.startAt
            else -> null
        }.toString()

        override fun provideEnd(event: Event): String = when (event) {
            is Event.WorkDay -> event.endAt
            else -> null
        }.toString()
    }
}