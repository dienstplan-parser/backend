package fp.dienstplan.parser.timezone

interface Timezone {
    fun build(stringBuilder: StringBuilder)
}