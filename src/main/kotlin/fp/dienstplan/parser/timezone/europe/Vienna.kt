package fp.dienstplan.parser.timezone.europe

import fp.dienstplan.parser.newLine
import fp.dienstplan.parser.timezone.Timezone


object TzVienna : Timezone {
    override fun build(stringBuilder: StringBuilder) {
        stringBuilder.newLine("BEGIN:VCALENDAR")
            .newLine("PRODID:-//tzurl.org//NONSGML Olson 2020b//EN")
            .newLine("VERSION:2.0")
            .newLine("BEGIN:VTIMEZONE")
            .newLine("TZID:Europe/Vienna")
            .newLine("LAST-MODIFIED:20201011T015911Z")
            .newLine("TZURL:http://tzurl.org/zoneinfo-outlook/Europe/Vienna")
            .newLine("X-LIC-LOCATION:Europe/Vienna")
            .newLine("BEGIN:DAYLIGHT")
            .newLine("TZNAME:CEST")
            .newLine("TZOFFSETFROM:+0100")
            .newLine("TZOFFSETTO:+0200")
            .newLine("DTSTART:19700329T020000")
            .newLine("RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU")
            .newLine("END:DAYLIGHT")
            .newLine("BEGIN:STANDARD")
            .newLine("TZNAME:CET")
            .newLine("TZOFFSETFROM:+0200")
            .newLine("TZOFFSETTO:+0100")
            .newLine("DTSTART:19701025T030000")
            .newLine("RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU")
            .newLine("END:STANDARD")
            .newLine("END:VTIMEZONE")
            .newLine("END:VCALENDAR")
    }
}