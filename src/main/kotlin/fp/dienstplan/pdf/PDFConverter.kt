package fp.dienstplan.pdf

import fp.dienstplan.util.launchProcess
import java.io.File
import kotlin.jvm.Throws

class PDFConverter(private val baseExecutable: String = "/usr/bin/pdftocairo") {

    class PDFConvertException(val output: MutableList<String>) : Exception()

    @Throws(PDFConvertException::class)
    fun run(file: File): File {
        val output = mutableListOf<String>()
        val exitCode = launchProcess(
            baseExecutable,
            file.absolutePath,
            "-png", "-singlefile",
            File(file.parentFile, file.nameWithoutExtension).absolutePath
        ) {
            output.add(it)
        }

        if (exitCode != 0) {
            throw PDFConvertException(output)
        }

        return File(file.parentFile, "${file.nameWithoutExtension}.png")
    }
}