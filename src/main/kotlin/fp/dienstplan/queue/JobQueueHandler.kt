package fp.dienstplan.queue

import fp.dienstplan.Logger
import fp.dienstplan.util.isPdf
import fp.dienstplan.ocr.OCRHelper
import fp.dienstplan.ocr.Plan
import fp.dienstplan.ocr.containsAny
import fp.dienstplan.pdf.PDFConverter
import fp.dienstplan.web.rest.v1.*
import kotlinx.coroutines.*
import java.io.File
import java.util.concurrent.LinkedBlockingDeque


class JobQueueHandler(
    private val concurrentJobs: Int,
    tesseractExecutable: String,
    pdfToCairoExecutable: String,
    private val errorLogDir: File,
) : Runnable {
    companion object {
        private val LOGGER = Logger("JobQueueHandler")
        private const val RESULT_CACHE_TIME_MS = 5 * 1000 * 60L

        private val WEEKDAYS_GERMAN = listOf(
            "Montag", "Dienstag", "Mittwoch", "Donnerstag",
            "Freitag", "Samstag", "Sonntag"
        )

        private const val PERIOD_LABEL_GERMAN = "Periode"
    }

    private val ocrHelper = OCRHelper<Plan, Plan.Builder>(
        baseExecutable = tesseractExecutable,
        addData = { builder, line ->
            if (line.containsAny(WEEKDAYS_GERMAN)) {
                builder.schedule.add(line)
            } else if (line.contains(PERIOD_LABEL_GERMAN)) {
                builder.setPeriod(line)
            }
        })

    private val pdfConverter = PDFConverter(pdfToCairoExecutable)

    private val jobs = MutableQueue<Job>()
    private val runningFutures = LinkedBlockingDeque<Deferred<JobStatus>>()

    fun addJob(job: Job) {
        LOGGER.print(Logger.Type.Info, "Job %s has been added", job.id)
        jobs.add(job)
    }

    fun getJob(id: String): Job? {
        return jobs.find {
            it.id == id
        }
    }

    fun removeJob(id: String) {
        jobs.remove { it.id == id }
    }

    override fun run() {
        while (true) {
            val now = System.currentTimeMillis()
            jobs.remove { job ->
                ((job.jobStatus is JobStatus.JobFinished) && job.jobStatus.timestamp + RESULT_CACHE_TIME_MS < now).also {
//                    if(it) LOGGER.print(Logger.Type.Info, "Job )
                }
            }

            if (runningFutures.size < concurrentJobs) {
                val nextJob = jobs.poll()
                if (nextJob != null) {
                    LOGGER.print(Logger.Type.Info, "Next job: %s", nextJob.id)
                    runningFutures.add(nextJob.start(ocrHelper, pdfConverter) {
                        LOGGER.print(
                            Logger.Type.Info,
                            "Job %s has finished (%s)",
                            nextJob.id, nextJob.jobStatus
                        )

                        if (nextJob.jobStatus is JobStatus.JobFailed) {
                            val failed = nextJob.jobStatus as JobStatus.JobFailed

                            File(errorLogDir, failed.errorFile).bufferedWriter().use { bw ->
                                failed.errorLog.forEach { line ->
                                    bw.write(line)
                                    bw.newLine()
                                }
                            }

                            LOGGER.print(Logger.Type.Error, "Job failed, saving error log (%s)", failed.errorFile)
                        }

                        runningFutures.remove(it)
                    })
                }
            }

            Thread.sleep(5000)
        }
    }
}

data class Job(
    val id: String,
    val tempDir: File,
    val fileExt: String
) {
    val file = File(tempDir, id + if (fileExt.startsWith(".")) fileExt else ".$fileExt")

    var jobStatus: JobStatus = JobStatus.JobCreated
        set(value) {
            this.statusUpdateHandler?.let { it(value) }
            field = value
        }

    var statusUpdateHandler: ((JobStatus) -> Unit)? = null

    @OptIn(DelicateCoroutinesApi::class, ExperimentalCoroutinesApi::class)
    fun start(
        ocrHelper: OCRHelper<Plan, Plan.Builder>,
        pdfConverter: PDFConverter,
        onFinished: (Deferred<JobStatus>) -> Unit
    ): Deferred<JobStatus> {
        val deferred: Deferred<JobStatus> = GlobalScope.async(Dispatchers.IO) {
            val convertedFile = if (file.isPdf()) {
                try {
                    pdfConverter.run(file)
                } catch (e: PDFConverter.PDFConvertException) {
                    return@async JobStatus.JobFailed(e.output, "${id}_${System.currentTimeMillis()}.error")
                } finally {
                    file.delete()
                }
            } else file

            try {
                val plan = ocrHelper.run(convertedFile, Plan.Builder())
                JobStatus.JobFinished(plan)
            } catch (e: OCRHelper.OCRException) {
                JobStatus.JobFailed(e.errorLog, "${id}_${System.currentTimeMillis()}.error")
            } finally {
                convertedFile.delete()
            }
        }

        deferred.invokeOnCompletion {
            jobStatus = deferred.getCompleted()
            onFinished(deferred)
        }

        jobStatus = JobStatus.JobRunning(deferred)
        return deferred
    }
}

sealed class JobStatus(private val state: State = State.CREATED, val timestamp: Long = System.currentTimeMillis()) {
    fun provideApiV1Metadata(id: String): ApiJobMetadataV1 {
        return ApiJobMetadataV1(
            id,
            state.toString(), timestamp
        )
    }

    abstract fun provideApiV1Response(id: String): Any

    object JobInvalid : JobStatus(State.INVALID) {
        override fun provideApiV1Response(id: String) =
            ApiJobInvalidResponseV1(this.provideApiV1Metadata(id))

    }

    object JobCreated : JobStatus(State.CREATED) {
        override fun provideApiV1Response(id: String) =
            ApiJobCreatedResponseV1(this.provideApiV1Metadata(id))
    }

    class JobRunning(deferred: Deferred<JobStatus>) : JobStatus(State.RUNNING) {
        override fun provideApiV1Response(id: String) =
            ApiJobRunningResponseV1(this.provideApiV1Metadata(id))
    }

    class JobFailed(val errorLog: List<String>, val errorFile: String) : JobStatus(State.FAILED) {
        override fun provideApiV1Response(id: String) =
            ApiJobFailedResponseV1(
                this.provideApiV1Metadata(id), ApiJobFailureDataV1(this.errorFile, this.errorLog)
            )
    }

    class JobFinished(val data: Plan) : JobStatus(State.FINISHED) {
        override fun provideApiV1Response(id: String) =
            ApiJobFinishedResponseV1(
                this.provideApiV1Metadata(id), ApiJobDataV1(
                    this.data.period.year, this.data.period.monthValue, this.data.schedule
                )
            )
    }

    enum class State {
        CREATED, RUNNING, FAILED, FINISHED, INVALID
    }
}