package fp.dienstplan.queue

import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

class MutableQueue<T> {
    private val backingList = ArrayList<T>()
    private var index = 0

    fun add(item: T) {
        synchronized(backingList) {
            backingList.add(item)
        }
    }

    fun poll(): T? {
        synchronized(backingList) {
            if (backingList.isEmpty() || (index != 0 && index >= backingList.size)) return null
            return backingList[index++]
        }
    }

    fun remove(predicate: (T) -> Boolean) {
        synchronized(backingList) {
            val set = TreeSet<Int>()
            for (i in 0 until backingList.size) {
                val item = backingList[i]
                if (predicate(item)) {
                    set.add(i)
                }
            }

            val it = set.descendingIterator()
            while (it.hasNext()) {
                val idxDelete = it.next()
                backingList.removeAt(idxDelete)
                if (idxDelete <= index) {
                    index--
                }
            }
        }
    }

    fun find(predicate: (T) -> Boolean): T? {
        synchronized(backingList) {
            return backingList.find(predicate)
        }
    }
}

@OptIn(DelicateCoroutinesApi::class)
fun main() {
    val queue = MutableQueue<String>()

    for (i in 0 until 10) {
        GlobalScope.launch {
            Thread.sleep(Random.nextLong(4000, 12000))
            val str = UUID.randomUUID().toString()
            println("($i) Adding new item to queue: $str")
            queue.add(str)
        }
    }


    GlobalScope.launch {
        while (true) {
            queue.remove { str ->
                val rand = Random.nextDouble()
                val remove = rand > 0.7
                if (remove) {
                    println("Removing $str ($rand)")
                }

                remove
            }

//            println(
//                "(Poll) List post remove is: ${
//                    queue.backingList.mapIndexed { index, t -> "$index: $t" }.joinToString(", ")
//                }"
//            )

            val sleep = Random.nextLong(2000, 7000)
            println("(Poll) Sleeping for $sleep")
            Thread.sleep(sleep)

            val str = queue.poll()
            if (str != null) {
                println("(Poll) Polled $str")
            }
        }
    }

    while (true) {
    }
}
