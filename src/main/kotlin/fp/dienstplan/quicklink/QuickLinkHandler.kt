package fp.dienstplan.quicklink

import kotlinx.coroutines.*
import java.util.*

class QuickLinkHandler(
    private val expireTimeMs: Long = 60 * 60 * 1000L
) {
    private val scope = CoroutineScope(Dispatchers.Default)
    private val quickLinks = mutableMapOf<String, Quicklink>()

    data class Quicklink(
        val content: String,
        val month: Int,
        val year: Int,
        val cleanupJob: Job,
    )

    fun generateQuickLink(content: String, month: Int, year: Int): String {
        val id = UUID.randomUUID().toString().substring(0, 8)
        val job = scope.launch {
            delay(expireTimeMs)
            synchronized(quickLinks) {
                quickLinks.remove(id)
            }
        }

        val quicklink = Quicklink(content, month, year, job)
        synchronized(quickLinks) {
            quickLinks[id] = quicklink
        }

        return id
    }

    fun getContent(id: String): Quicklink? {
        val quicklink = synchronized(quickLinks) {
            quickLinks.getOrDefault(id, null)
        } ?: return null

        return quicklink
    }
}