package fp.dienstplan.util

import io.javalin.http.ContentType

val ContentType.Companion.TEXT_CALENDAR: String
    get() = "text/calendar"