package fp.dienstplan.util

import com.fasterxml.jackson.module.kotlin.readValue
import fp.dienstplan.web.objectMapper
import java.io.File

fun File.isPdf(): Boolean {
    return this.extension == "pdf"
}

inline fun <reified T> File.toInstance() = objectMapper.readValue<T>(this.bufferedReader().use { it.readText() })