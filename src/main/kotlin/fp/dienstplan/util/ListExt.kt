package fp.dienstplan.util

fun <T> List<T>.toPair() = this[0] to this[1]