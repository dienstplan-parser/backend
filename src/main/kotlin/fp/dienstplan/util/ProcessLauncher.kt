package fp.dienstplan.util

import java.io.BufferedReader

fun launchProcess(vararg args: String): Process {
    return ProcessBuilder(*args).redirectErrorStream(true).start()
}

fun launchProcess(vararg args: String, lineCallback: (String) -> Unit): Int {
    return launchProcess(*args).also { process ->
        process.inputStream.bufferedReader().use { br -> br.readAll(lineCallback) }
    }.waitFor()
}

fun BufferedReader.readAll(fn: (String) -> Unit) {
    var line: String?
    while (this.readLine().also { line = it } != null) {
        fn(line!!)
    }
}