package fp.dienstplan.util

import com.fasterxml.jackson.databind.JsonNode
import fp.dienstplan.web.objectMapper

fun <T> mapBody(node: JsonNode, clazz: Class<out T>): T {
    return objectMapper.treeToValue(node, clazz)
}

