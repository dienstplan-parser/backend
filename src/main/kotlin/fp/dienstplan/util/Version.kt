package fp.dienstplan.util

val versionProperties = object {}::class.java.getResourceAsStream("/version.properties")?.bufferedReader()?.use {
    it.readLines()
}?.associate { it.split("=").toPair() }
