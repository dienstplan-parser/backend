package fp.dienstplan.web

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import fp.dienstplan.Logger
import fp.dienstplan.config.Config
import fp.dienstplan.web.api.http.GetJobStatusEndpoint
import fp.dienstplan.web.api.http.GetQuickLinkContentEndpoint
import fp.dienstplan.web.api.http.PostGenerateICalEndpoint
import fp.dienstplan.web.api.http.PostSubmitJobEndpoint
import fp.dienstplan.web.api.ws.PATH_PARAM
import fp.dienstplan.web.api.ws.WsJobUpdateListener
import fp.dienstplan.web.role.WebRole
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.core.compression.CompressionStrategy
import io.javalin.http.Context
import io.javalin.plugin.json.JavalinJackson
import org.koin.dsl.module
import javax.servlet.http.HttpServletResponse.SC_OK


val javalinModule = module {
    single {
        val logger = Logger("Javalin")

        val config = get() as Config

        Javalin.create { cfg ->
            cfg.showJavalinBanner = false
            cfg.compressionStrategy(CompressionStrategy.GZIP)
            cfg.requestLogger { ctx, executionTimeMs ->
                logger.print(
                    Logger.Type.Info, "%s %s -> %d (%s)  (took %.2fms)",
                    ctx.method(),
                    ctx.path(),
                    ctx.status(),
                    ctx.headerMap(),
                    executionTimeMs
                )
            }

            cfg.jsonMapper(JavalinJackson(objectMapper))

            cfg.accessManager { handler, ctx, roles ->
                WebRole.findSuitable(ctx, roles as Set<WebRole>)?.handle(handler, ctx)
                    ?: ctx.send(Response.InvalidAuth)
            }
        }.routes {
            path("job") {
                PostSubmitJobEndpoint

                path("{$PATH_PARAM}") {
                    WsJobUpdateListener
                }

                GetJobStatusEndpoint
            }

            path("calendar") {
                PostGenerateICalEndpoint
                GetQuickLinkContentEndpoint
            }

            get("/", okResp, WebRole.Default)
            head("health", okResp, WebRole.Default)

            if (config.debug) {
                before { ctx ->
                    ctx.header("Access-Control-Max-Age", "86400")
                    ctx.header("Access-Control-Allow-Origin", "*")
                    ctx.header("Vary", "Origin")
                }
            }
        }.apply {
            if (config.debug) {
                this.options("job", okResp, WebRole.Default)
                this.options("job/*", okResp, WebRole.Default)
            }
        }
    }
}

val okResp: (Context) -> Unit = { it.status(SC_OK) }

val objectMapper: ObjectMapper = JavalinJackson.defaultMapper().registerModule(JavaTimeModule())

