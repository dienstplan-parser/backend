package fp.dienstplan.web

import io.javalin.http.Context
import io.javalin.websocket.WsContext
import java.net.HttpURLConnection.HTTP_BAD_REQUEST

sealed class Response(private val text: String, private val status: Int) {
    open class WithErrorResponse(text: String, status: Int, private val errorText: String?) : Response(text, status) {
        override fun toMap() = super.toMap().apply {
            if (errorText != null) this["error"] = errorText
        }
    }

    object InvalidAuth : Response("Invalid auth", HTTP_BAD_REQUEST)

    class InvalidBody(errorText: String? = null) : WithErrorResponse("Invalid body data", HTTP_BAD_REQUEST, errorText)

    class InvalidParameter(errorText: String? = null) :
        WithErrorResponse("Invalid body data", HTTP_BAD_REQUEST, errorText)

    object InvalidApiVersionHeader : Response("Invalid Api version header", HTTP_BAD_REQUEST)


    open fun toMap(): MutableMap<String, String> {
        return mutableMapOf("response" to text)
    }

    fun send(ctx: Context) {
        ctx.status(status).json(toMap())
    }

    fun send(wsCtx: WsContext) {
        wsCtx.send(toMap())
    }
}

fun Context.send(response: Response) = response.send(this)
