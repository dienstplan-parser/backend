package fp.dienstplan.web.api.http

import fp.dienstplan.queue.JobQueueHandler
import fp.dienstplan.queue.JobStatus
import fp.dienstplan.web.Response
import fp.dienstplan.web.api.http.base.path.ApiPathParam1Endpoint
import fp.dienstplan.web.api.http.base.path.StringPathParamValidator
import fp.dienstplan.web.api.http.base.rest.RestApiVersion
import fp.dienstplan.web.api.http.base.rest.apiVersion
import fp.dienstplan.web.rest.v1.*
import fp.dienstplan.web.role.WebRole
import io.javalin.http.Context
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

const val JOB_ID_PARAM = "id"

object GetJobStatusEndpoint : ApiPathParam1Endpoint<String>(
    paths = arrayOf("{$JOB_ID_PARAM}"),
    roles = arrayOf(WebRole.Default),
    xValidator = StringPathParamValidator(JOB_ID_PARAM)
), KoinComponent {

    private val jobQueueHandler by inject<JobQueueHandler>()

    override fun handle(ctx: Context, id: String) {
        if (ctx.apiVersion() == RestApiVersion.V1) {
            val job = jobQueueHandler.getJob(id)
            if (job != null) {
                val metadata = job.jobStatus.provideApiV1Metadata(job.id)
                val json: Any = when (val status = job.jobStatus) {
                    JobStatus.JobCreated -> ApiJobCreatedResponseV1(metadata)
                    is JobStatus.JobFailed -> ApiJobFailedResponseV1(
                        metadata, ApiJobFailureDataV1(
                            status.errorFile, status.errorLog
                        )
                    )
                    is JobStatus.JobFinished -> ApiJobFinishedResponseV1(
                        metadata, ApiJobDataV1(
                            status.data.period.year, status.data.period.monthValue, status.data.schedule
                        )
                    )
                    is JobStatus.JobRunning -> ApiJobRunningResponseV1(metadata)
                    is JobStatus.JobInvalid -> ApiJobInvalidResponseV1(metadata)
                }

                ctx.json(json)
            }
        } else {
            Response.InvalidApiVersionHeader.send(ctx)
        }
    }
}