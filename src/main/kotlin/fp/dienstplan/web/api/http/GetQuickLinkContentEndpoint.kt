package fp.dienstplan.web.api.http

import fp.dienstplan.quicklink.QuickLinkHandler
import fp.dienstplan.web.Response
import fp.dienstplan.web.api.http.base.path.ApiPathParam1Endpoint
import fp.dienstplan.web.api.http.base.path.StringPathParamValidator
import fp.dienstplan.web.rest.v2.ApiQuickLinkResponseV2
import fp.dienstplan.web.role.WebRole
import io.javalin.http.Context
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

const val QUICKLINK_ID_PARAM = "id"

object GetQuickLinkContentEndpoint : ApiPathParam1Endpoint<String>(
    paths = arrayOf("{$QUICKLINK_ID_PARAM}"),
    roles = arrayOf(WebRole.Default),
    xValidator = StringPathParamValidator(QUICKLINK_ID_PARAM)
), KoinComponent {
    private val quickLinkHandler by inject<QuickLinkHandler>()

    override fun handle(ctx: Context, x: String) {
        val quicklink = quickLinkHandler.getContent(x)
        if (quicklink == null) {
            Response.InvalidParameter("This quicklink does not exist!").send(ctx)
            return
        }

        ctx.json(
            ApiQuickLinkResponseV2(
                quicklink.content, quicklink.month, quicklink.year
            )
        )
    }
}