package fp.dienstplan.web.api.http

import fp.dienstplan.parser.DienstplanParser
import fp.dienstplan.parser.output.ical.ICalBuilder
import fp.dienstplan.parser.output.json.JsonBuilder
import fp.dienstplan.quicklink.QuickLinkHandler
import fp.dienstplan.web.Response
import fp.dienstplan.web.api.http.base.rest.ApiRestEndpoint
import fp.dienstplan.web.api.http.base.rest.RestApiVersion
import fp.dienstplan.web.api.http.base.rest.apiVersion
import fp.dienstplan.web.rest.stub.IApiJobData
import fp.dienstplan.web.rest.v1.ApiCalendarCreatedResponseV1
import fp.dienstplan.web.rest.v1.ApiJobDataV1
import fp.dienstplan.web.rest.v2.ApiCalendarCreatedResponseV2
import fp.dienstplan.web.role.WebRole
import io.javalin.core.util.Header
import io.javalin.http.ContentType
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*
import fp.dienstplan.util.TEXT_CALENDAR

object PostGenerateICalEndpoint : ApiRestEndpoint<IApiJobData>(
    mapOf(
        RestApiVersion.V1 to ApiJobDataV1::class.java,
        RestApiVersion.V2 to ApiJobDataV1::class.java
    ), roles = arrayOf(WebRole.Default),
    method = HandlerType.POST
), KoinComponent {

    private val quickLinkHandler by inject<QuickLinkHandler>()

    private val createIcalBuilder = { ICalBuilder() }

    private val outputBuilders = mapOf(
        ContentType.TEXT_CALENDAR to createIcalBuilder,
        ContentType.APPLICATION_JSON.mimeType to { JsonBuilder() }
    )


    override fun handle(ctx: Context, body: IApiJobData) {
        val builder = ctx.header(Header.ACCEPT)?.let { outputBuilders[it] } ?: createIcalBuilder
        if (body is ApiJobDataV1) {
            val text = DienstplanParser.parse(
                body.schedule,
                body.month,
                body.year,
                builder = builder()
            )

            val resp = when (ctx.apiVersion()) {
                RestApiVersion.V1 -> ApiCalendarCreatedResponseV1(text)
                RestApiVersion.V2 -> {
                    ApiCalendarCreatedResponseV2(text, quickLinkHandler.generateQuickLink(text, body.month, body.year))
                }
            }

            ctx.json(resp)
        } else {
            Response.InvalidApiVersionHeader.send(ctx)
        }
    }
}
