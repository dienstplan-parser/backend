package fp.dienstplan.web.api.http

import fp.dienstplan.config.Config
import fp.dienstplan.queue.Job
import fp.dienstplan.queue.JobQueueHandler
import fp.dienstplan.web.Response
import fp.dienstplan.web.api.http.base.ApiEndpoint
import fp.dienstplan.web.role.WebRole
import io.javalin.core.util.FileUtil
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

object PostSubmitJobEndpoint : ApiEndpoint(method = HandlerType.POST, roles = arrayOf(WebRole.Default)), KoinComponent {
    private const val FORM_FILE_PRIVACY_CONSENT = "privacy_consent"
    private const val FORM_FILE_FIELD_NAME = "file"
    private val allowedFormats = listOf("jpeg", "jpg", "png", "pdf")

    private val config by inject<Config>()
    private val jobQueueHandler by inject<JobQueueHandler>()

    override fun handle(ctx: Context) {
        val consent = ctx.formParam(FORM_FILE_PRIVACY_CONSENT)?.toBoolean()
        val uploadedFile = ctx.uploadedFile(FORM_FILE_FIELD_NAME)

        if (consent != true) {
            Response.InvalidBody("No consent to process data given").send(ctx)
            return
        }

        if (uploadedFile == null) {
            Response.InvalidBody("No file uploaded").send(ctx)
            return
        }

        if (uploadedFile.extension.substring(1) !in allowedFormats) {
            Response.InvalidBody("Invalid file format").send(ctx)
            return
        }

        if (uploadedFile.size > config.maxFileSizeBytes) {
            Response.InvalidBody("File too big").send(ctx)
            return
        }

        val job = Job(UUID.randomUUID().toString(), config.tempScanDir, uploadedFile.extension)

        FileUtil.streamToFile(uploadedFile.content, job.file.absolutePath)
        jobQueueHandler.addJob(job)

        ctx.json(job.jobStatus.provideApiV1Response(job.id))
    }
}

