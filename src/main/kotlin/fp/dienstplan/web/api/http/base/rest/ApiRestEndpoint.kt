package fp.dienstplan.web.api.http.base.rest

import fp.dienstplan.web.Response
import fp.dienstplan.web.api.http.base.ApiEndpoint
import fp.dienstplan.web.send
import io.javalin.core.security.RouteRole
import io.javalin.core.validation.BodyValidator
import io.javalin.core.validation.ValidationException
import io.javalin.http.Context
import io.javalin.http.HandlerType

abstract class ApiRestEndpoint<T>(
    private val classes: Map<RestApiVersion, Class<out T>>,
    vararg path: String,
    roles: Array<RouteRole>,
    method: HandlerType
) : ApiEndpoint(*path, roles = roles, method = method) {

    companion object {
        const val REST_API_VERSION_NAME = "Rest-Api-Version"
    }

    override fun handle(ctx: Context) {
        classes[ctx.apiVersion()]?.let { clazz ->
            ctx.bodyValidator(clazz).getOrNull()?.let { body ->
                this.handle(ctx, body)
            } ?: Response.InvalidBody().send(ctx)
        } ?: Response.InvalidApiVersionHeader.send(ctx)
    }

    abstract fun handle(ctx: Context, body: T)
}

fun Context.apiVersion() = RestApiVersion.find(this.header(ApiRestEndpoint.REST_API_VERSION_NAME))
    ?: RestApiVersion.latest()


fun <T> BodyValidator<T>.getOrNull(): T? {
    //this used to be a thing in Javalin 3.x.x, but is not anymore for some bizarre reason
    return try {
        this.get()
    } catch (e: ValidationException) {
        null
    }
}


enum class RestApiVersion {
    V1, V2;

    companion object {
        fun find(version: String?) = values().find {
            it.name.equals(version, ignoreCase = true)
        }

        fun latest() = values().last()
    }
}