package fp.dienstplan.web.api.ws

import fp.dienstplan.queue.JobQueueHandler
import fp.dienstplan.queue.JobStatus
import fp.dienstplan.Logger
import fp.dienstplan.web.api.http.base.path.StringPathParamValidator
import fp.dienstplan.web.api.ws.base.PathParam1Endpoint
import fp.dienstplan.web.role.WebRole
import io.javalin.websocket.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

const val PATH_PARAM = "id"

object WsJobUpdateListener : PathParam1Endpoint<String>(
    roles = arrayOf(WebRole.Default), xValidator = StringPathParamValidator(PATH_PARAM)
), KoinComponent {
    private val LOGGER = Logger("WsJobUpdateListener")
    private val jobQueueHandler by inject<JobQueueHandler>()

    override fun handleConnect(ctx: WsConnectContext, id: String) {
        val job = jobQueueHandler.getJob(id)
        if (job != null) {
            val sendFn: (JobStatus) -> Unit = { status ->
                LOGGER.print(Logger.Type.Info, "Pushing status update $status to $ctx")

//                ctx.send(
//                    JobStatus.JobFailed(
//                        listOf(
//                            "Lorem ipsum dolor sit amet, volumus accusam ei eum, tale vivendo mandamus ex cum.",
//                            "In sed nusquam disputando signiferumque. Tempor epicurei suavitate vix id.",
//                            "Ad purto mazim vix. Ex eum sale commune, an dolorem volumus denique mei.",
//                            "Dolor praesent eos id, eam blandit democritum an, etiam debet pro ut.",
//                            "Cu homero expetenda est, at nemore philosophia vituperatoribus vis. Te usu regione nostrum. Per eu zril disputando.",
//                            "Mel diam pericula iracundia at, denique dolores democritum has eu. Sit no quodsi sanctus.",
//                            "Ius eu iriure alienum, ius ne illum tollit, in quem graeco oblique sea.",
//                            "Ipsum corpora nominavi duo id, rebum utinam vis ei. Vel no unum facete equidem, saepe deseruisse pro ne.",
//                            "His no doctus fastidii, id mea soluta cetero petentium. Ignota aliquam nec et, ei esse platonem conceptam his."
//                        )
//                    ).provideApiV1Response(id)
//                )

                ctx.send(status.provideApiV1Response(job.id))

//                if (job.jobStatus is JobStatus.JobFinished) {
//                    LOGGER.print(Logger.Type.INFO, "Job received by client, removing..")
//                    JobQueueHandler.getInstance().removeJob(id)
//                }
            }

            //force push to client on connect (on page refresh to instantly receive current state [since no further updates will be sent if job has already finished])
            sendFn(job.jobStatus)
            job.statusUpdateHandler = sendFn
        } else {
            LOGGER.print(Logger.Type.Info, "No job found for id $id")
            ctx.send(JobStatus.JobInvalid.provideApiV1Response(id))
        }
    }

    override fun handleMessage(ctx: WsMessageContext, id: String) {

    }

    override fun handleBinaryMessage(ctx: WsBinaryMessageContext, x: String) {
    }

    override fun handleClose(ctx: WsCloseContext, id: String) {
        val job = jobQueueHandler.getJob(id)
        if (job != null) {
            job.statusUpdateHandler = null
            LOGGER.print(Logger.Type.Info, "Removing handler for $id")
        } else {
            LOGGER.print(Logger.Type.Info, "Socket closed but no job exists for $id")
        }
    }

    override fun handleError(ctx: WsErrorContext, x: String) {
    }
}
