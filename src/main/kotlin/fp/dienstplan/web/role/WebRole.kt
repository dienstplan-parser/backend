package fp.dienstplan.web.role

import io.javalin.core.security.RouteRole
import io.javalin.http.Context
import io.javalin.http.Handler

enum class WebRole : RouteRole {
    Default {
        override fun isSuitable(ctx: Context) = true

        override fun handle(handler: Handler, ctx: Context) {
            handler.handle(ctx)
        }
    };

    abstract fun isSuitable(ctx: Context): Boolean
    abstract fun handle(handler: Handler, ctx: Context)

    companion object {
        fun findSuitable(ctx: Context, roles: Set<WebRole>) = roles.find {
            it.isSuitable(ctx)
        }
    }
}